<?php
/**
*	@author Guilherme Soares;
*	 SEO - Application generator
*	@copyright Public Reserved by ifSystem at:
*	10 March of 2013
*	@version Version 1.0.0.0.2v
*
*	@license:
*
*	The project will help all developers in the industry SEO website with keywords and description, 
*	data analysis, among others
*
*	# Open modules start all services if exec others commands after this print ERROR_SEO_1
*	main();
*
*
*/

class SeoMarket {

	public static function __callStatic($f,$a){
		
		if( SeoLib::$init || ($f == 'main') || ($f == 'open') ){

			/* Before execute call */
			call_user_func_array("SeoLib::actionBefore",array($f,$a, null));

			/* Execute function */
			$return = call_user_func_array("SeoLib::{$f}",$a);
			$return = (!isset($return)) ? null : $return;
				
			/* After execute call */
			call_user_func_array("SeoLib::actionAfter",array($f,$a,$return));
			return $return;
		}else{

			return SeoError::log(ERROR_SEOMARKETING_001);
		}
	}

}
SeoMarket::main();
?>