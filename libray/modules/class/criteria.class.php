<?php
/*
	Seo Criteira is developer conditions
	Version 1.0.0.0.0.0.0
	Developer: Guilherme Soares
	Date: 11:56 26 March of 2013
*/
class SeoCriteria{

	const 
	
	 __IF__ = 'if',

	 __ELSEIF__ = 'elseif',
	 
	 __ELSE__ = 'else',
	 
	 __OR__ = '||',
	 
	 __AND__ = '&&',
	 
	 __XOR__ = 'xor',
	 
	 __EQUAL__ = '==',
	 
	 __UNEQUAL__ = '!=',

	 __ADD__ = '=',
	 
	 __IDENTICAL__ = '===',
	 
	 __UNIDENTICAL__ = '!==',
	 
	 __AS__ = '\'',
	 
	 __AS2__ = '"',
	 

	 __CLOSE__ = ')',
	 
	 ___OPEN__ = '(';

	protected $group = array();

	public function __construct($v1, $v2, $logic = null, $operator = '=='){
	
		$this->add($v1, $v2, $logic, $operator);
	}

	public function add($v1, $v2, $logic = null, $operator = '=='){
	
		return $this->group[] = self::___OPEN__
								 . $v1
								 . $operator
								 . $v2
							    . self::__CLOSE__ 
							    . (empty($logic) ? '' : $logic);
	}

	public function get(){
	
		return self::___OPEN__ 
				. implode('', $this->group) 
			   . self::__CLOSE__;
	}
}
?>