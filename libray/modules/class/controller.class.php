<?php
/*
	Seo Controller
	Version 1.0.0.0.0.0.0
	Developer: Guilherme Soares
	Date: 11:11 29 May of 2013
*/
class SeoController{
	/**
	 * Framework ele trabalha com buffers sendo assim só vai ser liberado a saida de algum conteudo no final de chamar todos os Model, Views que é para serem executados
	*/
	private $buffer = '';

	//Adiciona um View em uma página
	public function addView($name){
		ob_start();
			SeoView::addView($name);
			$this->buffer .= ob_get_contents();

		ob_end_clean();
	}

	//Adiciona um Model em uma página
	public function addModel($name){
		
		ob_start();
			SeoModel::addModel($name);
			$this->buffer .= ob_get_contents();
		ob_end_clean();
	}
	

	//Retorna o Controller que esta sendo utilizado
	public function getCurrentController(){

		return str_replace('Controller', '', __CLASS__);
	}

	//Retorna Buffer da página com models, view que forma escritos
	public function getBuffer($print = true){
		
		if($print)
			echo $this->buffer;

		return $this->buffer;
	}

	public function getLogAction(){
		$this->cleanBuffer();
		$this->getLog();
		header('Content-type: text/html; charset=UTF-8');
		$log = SeoError::getLog();
		echo $log;
	}
	
	public function setTitle($title = null){

		$this->buffer .= '<title>' . $title . '</title>';
	}

	public function createLink($page, $action = '', $linkExists = true){
		
		$XMLName =  empty($action) ? 'Link' : $action;
		$XMLName = $page . $XMLName;


		$TextLink = SeoLanguage::printText('menu', $XMLName, false);

		$TemplateLink = SeoTemplate::getTemplate('Link');

		$link = "/$page/$action";
		
		if(!$linkExists)
			$link = '';
		
		$HTMLLink = str_replace(
						array('[LINK]', '[TEXTLINK]'), 
						array($link, $TextLink), 
						$TemplateLink);


		/*
		<a href="/[page]/[action]" title="[TEXTLINK]">[TEXTLINK]</a>

		 <a href="/Distribuidores/NegociosDiversificados" title="<?php SeoLanguage::printText('menu', 'DistriuidoresNegocios')?>">
             <?php SeoLanguage::printText('menu', 'DistriuidoresNegocios')?>
          </a>
		*/

        echo $HTMLLink;
	}

	public function cleanBuffer(){

		$this->buffer = "";
	}

	//Mostra a página de ERROR
	public function Error($num = 0){

		$this->buffer = null;
		switch($num){
			default:
			case 404:

				$this->addView('Error/index');
			break;
		}
	}

	public function Soon(){

		$this->buffer = null;
		$this->addView('EmBreve/index');
	}


	/************* Event ************/
	/**
	 *@public funtion beforeBuffer(){},                    ///Antes de Ter qualquer Buffer 
	 *@public funtion beforeGetBuffer(){}, 				   ///Antes de colocar na tela Buffer
	 *@public funtion afterGetBuffer(){},                  ///Depois de Colocar na Tela o Buffer
	 *
	*/
	public function beforeBuffer(){}
	public function beforeGetBuffer(){}
	public function afterGetBuffer(){}
	/************ Event ************/
}?>