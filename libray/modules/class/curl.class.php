<?php

class SeoCurl {

   public static function request($url = false, $implents = array()){

        if(!$url)
            return false;

        $ch = curl_init($url);
        
        foreach($implents as $name => $val){

            curl_setopt($ch, $name, $val);
        }

        $result = curl_exec($ch);
        $info = curl_getinfo($ch);
        curl_close($ch);
        return ($info['http_code']!=200) ? false : $result;
   }

}
?>