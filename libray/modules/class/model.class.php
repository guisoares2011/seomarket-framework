<?php
/*
	SeomModels - includes content on page with default MVC
	Use controllers and models and views

	Version 1.0
*/
class SeoModel{

	#All include by SeoModels is phtml
	#Is different not call file..]
	const ext = '.php';

	#Save all files include
	private static $includes = array();

	#if check return you include this file
	public static function check($name){

		foreach(self::$includes as $include){

			if($name == $include)
				return true;
		}

		return false;
	}

	public static function addModel($file){

		$name = APPLICATION_PATH . MODELS_PATH . $file . self::ext;


		if(!self::check($file) && file_exists($name)){

			require($name);
			self::$includes[] = $file;
		
		}else{
			return SeoError::cry(ERROR_SEOMARKETING_022, array(
															'model'=>$file)
			);
		}
	}
}
?>