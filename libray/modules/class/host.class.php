<?php
/*
	SeoHost - Get Host and make a full anchor
	Version 1.0
*/
class SeoHost{

	private static $host;
	private static $protocol;
	private static $port;
	private static $port_iguinore = '80';

	#GEt current Host
	public static function  getHost(){
		
		return self::$host = $_SERVER["SERVER_NAME"];
	}

	#Get a full URL
	public static function getUrl(){
		
		self::getPort();
		self::getProtocol();
		self::getHost();

		return self::$protocol . self::$host . self::$port . '/';	
	}

	#Get a port curretn
	public static function getPort(){

		$port = $_SERVER["SERVER_PORT"];

		#Verify if port iguinore is equal a current port
		if($port == self::$port_iguinore){

			return self::$port = null;
		}else{

			return self::$port = ':' . $port;
		}
	}

	#Get protocol return http or https
	public static function getProtocol(){
		
		if(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on'){

			return self::$protocol = 'https://';
		}else{

			return self::$protocol = 'http://';
		}
	}

	#Return full links
	#Use in URL Friendly
	public static function section($anchor = "", $print = true){
		
		$a = self::getUrl() . $anchor;
		if($print) echo $a;
		
		return $a;
	}
}
?>