<?php

class SeoHeaders {

   private 

   $request = null,

   $open = false;

   const 
    
    NT = "\r\n",
    
    ATTR_VAL = ': ',

    HTTP = 'HTTP/1.1';

   public function open($dates, $method = 'GET'){

        $this->open = true;
        $this->request = $method . ' ' . $dates . ' '. self::HTTP . self::NT; 
        return $this;
   }

   public function insert($name, $value){

        $this->request .= $name . self::ATTR_VAL . $value . self::NT;  
        return $this;
   }

   public function get(){

        return ($this->open) ? $this->request : false;
   }

   public function close(){

        $a = $this->get();
        $this->request = null;
        $this->open = false;

        return $a;
   }
}
?>