<?php
/*
	SEO -  Trait Selector
	Public Reserved by ifSystem at:
	10 March of 2013
	Version 1.0v
*/
class SeoDom{

	public static $dom = false;

	const 
		
		currentNode = '$current->nodeName',

		currentAttr = '$current->getAttribute',

		callback = '{$els[\'length\']++; $els[\'item\'][] = $current;}'
	;

	public static function newDom ($file){

		self::$dom = new DOMDocument('1.0');
		self::$dom->resolveExternals = true;
		
		$content = SeoFile::getContents($file);
		
		if(!$content)
			return false;

		/*
			Use @ because files is not valid XHTML, print ERROR
			With @ not print this errors..

		*/
		
		@self::$dom->loadHTML($content);

		return self::$dom;
	}

	public static function requireDOM(){

		if(!self::$dom){

			return SeoError::log(ERROR_SEOMARKETING_008);
		}
		return true;
	}

	public static function querySelector($tags){

		$return = self::querySelectorAll($tags);		
		return ($return->length > 0) ? $return->item[0] : null;
	}

	public static function querySelectorAll($tags){

		$tags = explode(',',$tags);
		
		$tag_array = array();
		
		foreach($tags as $num => $tag){

			$tag = preg_replace('/["\']/','',$tag);
			
			if(preg_match_all('/(\[[^\[\]]+\])/', $tag, $match)){
				
			
				$t = $tag_array[ $num ][ 'name' ] = $tag;	
				foreach($match[0] as $code => $attr){

					$t = str_replace($attr, '', $t);

					$attr = preg_replace('/[\[\]]/','',$attr);

					if(strpos($attr, SeoCriteria:: __UNEQUAL__)){

						$spaces = explode(SeoCriteria:: __UNEQUAL__, $attr);
						$operator = SeoCriteria:: __UNEQUAL__;
					}else{

						$spaces = explode(SeoCriteria:: __ADD__, $attr);
						$operator = SeoCriteria:: __EQUAL__;
					}
					


					$tag_array[ $num ][ 'attr' ][ $code ] = array(
									'name' => $spaces[0],
									'value' => $spaces[1],
									'operator' => $operator); 
				}
			
				$tag_array[ $num ][ 'name' ] = $t;		
			}else{
				
				$tag_array[$num]['name'] = trim($tag);
			}
		}
		return self::researchTags($tag_array);
	}

	public static function text($dom){
		
		if(isset($dom->nodeType) && $dom->nodeType == 1){
		
			return $dom->textContent;
		}
		return false;
	}

	public static function html($dom){
		
		if(isset($dom->nodeType) && $dom->nodeType == 1){
		
			return $dom->nodeValue;
		}
		return false;
	}

	public static function attr($dom, $attr){
		
		if(isset($dom->nodeType) && $dom->nodeType == 1){
		
			return $dom->getAttribute($attr);
		}
		return false;
	}


	public static function researchTags($tag){



		$con = self::createIf($tag);
		$els = array('length'=>0,'item'=>array());

		if(!self::requireDOM())
			return false;

		$t = self::$dom->getElementsByTagName('*');
		$length = $t->length;
		
		for($i = 0; $i < $length ; $i++){
		
			$current = $t->item($i);
			eval($con);
		}
		return (object)$els;
	}

	public static function createIf($resources){
		$query = '';

		foreach($resources as $key => $el){

			$query .= ($key > 0) ? 
						SeoCriteria::__ELSEIF__ : 
						SeoCriteria::__IF__;

			if(array_key_exists('attr', $el)){
				
				if($el['name'] == '*'){
					
					$e = new SeoCriteria(	
						SeoCriteria::__AS__ . '1' . SeoCriteria::__AS__, 
						SeoCriteria::__AS__ . '1' . SeoCriteria::__AS__, 
						SeoCriteria::__AND__
					);

				}else{
					$e = new SeoCriteria(
					self::currentNode, 
					SeoCriteria::__AS__ . $el['name'] . SeoCriteria::__AS__, 
					SeoCriteria::__AND__
					);
				}
				
				$length = count($el['attr']) - 1;

				foreach($el['attr'] as $key2 => $attr)
					if($length == $key)		
						$e->add(
							 self::currentAttr 
							  . SeoCriteria::___OPEN__
							   . SeoCriteria::__AS2__ . $attr['name'] . SeoCriteria::__AS2__
							  . SeoCriteria::__CLOSE__,
							 SeoCriteria::__AS__ . $attr['value'] . SeoCriteria::__AS__,
							 null,
							 $attr['operator']);
					else
						$e->add(
							self::currentAttr 
							  . SeoCriteria::___OPEN__
							   . SeoCriteria::__AS__ . $attr['name'] . SeoCriteria::__AS__
							  . SeoCriteria::__CLOSE__,
							 SeoCriteria::__AS__   . $attr['value']  . SeoCriteria::__AS__,
							 SeoCriteria::__AND__,
							 $attr['operator']);
			}else
				if($el['name'] == '*')
					$e = new SeoCriteria(
								SeoCriteria::__AS__ . '1' . SeoCriteria::__AS__, 
								SeoCriteria::__AS__ . '1' . SeoCriteria::__AS__);
				else
					$e = new SeoCriteria(
							self::currentNode, 
					 		SeoCriteria::__AS__ . $el['name'] . SeoCriteria::__AS__);
				
			
			$query .= $e->get() . self::callback;
		}
		return $query;
	}

	public static function str_attr($name = array(), $val = ''){
		
		$q = '';
		if(is_array($name)){
		
			foreach($name as $attr => $v){
		
				$q .= "[$attr=$v]";
			}
		}else{
		
			$q .= "[$name=$val]";
		}
		return $q;
	}

	public static function getHeaders(){
		
		return self::querySelectorAll('h1,h2,h3,h4,h5');
	}

	public static function getLinks(){
		
		return self::querySelectorAll('a');
	}

	public static function getLinksNofollow(){

		return self::querySelectorAll('a[rel=nofollow]');
	}

	public static function getLinksfollow(){

		return self::querySelectorAll('a[rel!=nofollow]');
	}
	
	public static function getMetaName($name){
		
		$a = self::querySelectorAll('meta[name='. $name . ']');
		
		if($a->length == 0)
			return null;
		else
			return trim($a->item[0]->getAttribute('content'));
	}

}
?>