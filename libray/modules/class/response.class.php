<?php
/*
	Seo Response - Add to logs
	Public Reserved by ifSystem at:
	10 March of 2013
	Version 1.0v

*/

class SeoResponse{

	private static $response = array();

	public static function addResponse($_LOG, $resources){

		$current = self::lengthResponse();
		self::$response[ $current ][ 'log' ] = $_LOG;
		
		foreach($resources as $name => $value){

			self::$response[ $current ][ $name ] = $value;
		} 
	}

	public static function removeResponse($id = -1){

		if($id == -1)
			return false;

		if(array_key_exists($id, self::$response))
			unset(self::$response[$id]);		
	}

	public static function getResponse($id = -1){

		if($id == -1)
			return self::$response;

		if(array_key_exists($id, self::$response))
			return self::$response[ $id ];
	}

	public static function lengthResponse(){
		
		return count(self::$response);
	}
}
?>