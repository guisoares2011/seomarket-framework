<?php
/**
* @ SeoXML - Parse Files XML
* @param Atom and RSS feeds
* @param Add articles in feeds
* @param $elete..
*/

/*
<?xml version="1.0"?>
<feed xmlns="http://www.w3.org/2005/Atom">
<title>Feeds Tittle</title>
  <updated><TIME></updated>
  <author>
    <name>W3C Mailing List Archiver</name>
  </author>
  <entry>
    <title>Automotive Business Group</title>
    <author>
      <name><!--- Name Author ---></name>
      <email><!--- Email Author ---></email>
    </author>
    <id>mid:51680E56.1000606@w3.org</id>
    <updated>TIME GTM</updated>
    <content type="xhtml">![CDATA[	
    	<div xmlns="http://www.w3.org/1999/xhtml">---html---</div>
    ]]>
    </content>
    <content type="text"><![CDATA[----text----]]></content>
  </entry>
</feed>
*/
class SeoXML{

	#Read RSS feed
	private static $rss = true;
	
	#File xml
	private static $xml = null;
	
	#Read ATOM feed
	private static $atom = true;
	
	#All content XML
	private static $content = null;

	#Number de results to print
	private static $number = 5;

	#Return time GMT
	public static function GMT(){

		return gmdate("M d Y H:i:s");
	}

	#REturn is file set is xml or not
	public static function isXML($file){

		return ('xml' == pathinfo($file, PATHINFO_EXTENSION));
	}

	#Set xml to class
	public static function setXML($file){

		if(!self::isXML($file))
			return false;

		self::$xml = $file;
		self::$content = SeoFile::getContents($file); 
	}

	#Parse Array or Json to atom rss feeds
	public static function parse_to_atom($array, $html = false){
		
	}

	public static function str_abstract($string, $maxlength){
		
		preg_match_all('/^(.{' . $maxlength . ',}\s)/', $string, $match);
		return $match[0] . '.';	
	}

}
?>