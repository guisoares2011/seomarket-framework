<?php
/*
	SeoFile = Create, delete and alter files
	Public Reserved by ifSystem at:
	10 March of 2013
	Version 1.0v

*/

class SeoFile{
	
	public static function setFile($file){

		if(!file_exists($file) && is_file($file))
			return die(SeoError::log(ERROR_SEOMARKETING_007 . $file));
		
		SeoDom::newDom($file);

		$a = SeoDom::querySelector('body');
		
		SeoLib::setSystem('file',
			
			array(

			 'doctype' => SeoDom::querySelector('body')->ownerDocument->doctype->internalSubset,

			 'tags' => SeoDom::querySelectorAll('*'),
			 
			 'text' => SeoLib::removeSpaces( SeoDom::text( SeoDom::querySelector('body') ) ),

			 'headers' => SeoDom::getHeaders(),

			 'links' => array(

			 	'all' => SeoDom::getLinks(),

			 	'nofollow' => SeoDom::getLinksNofollow(),

			 ),

			 'keywords' => SeoDom::getMetaName('keywords'),

			 'description' => SeoDom::getMetaName('description'),
		));

		if(self::is_url($file)){

			$google = new SeoGoogle($file);
			
			SeoLib::setSystem('file',
				array(
					'seachers' => array(
						'google' => 
							array(
								'pagerank' => $google->getPageRank(),
							)
					),
					'social' => array(
						'google' => array(

						),
						'facebook' => array(

						)
						)
				)
			);
		}
	}

		#Get content of file
	public static function getContents($file){
		if(self::is_url($file)){
			
			ob_start();
       			if(!@include ($var)){
       				//Don't work try method
       				
       				$file_contents = SeoCurl::request($file, array(
						CURLOPT_URL => $file,
						CURLOPT_RETURNTRANSFER => 1,
						CURLOPT_CONNECTTIMEOUT => 15
					));

					if(!$file_contents){

						$file_contents = @file($file);
						$file_contents = implode('', $file_contents);
					}

					echo $file_contents;
       			}

        	$content = ob_get_clean();
		}else{

			if(is_file($file) && file_exists($file)){	

				$content = file_get_contents($file);
			}else{

				//I can't..Sorry, but don't cry..
				return false;
			}
		}

		return $content;
	}

	#Create or alter content of file
	public static function writeFile($file, $content, $method = null){

		if(file_exists($file)){

			$contentFile = self::getContents($file);	
		}else{

			$contentFile = '';
		} 

		$a = fopen($file, "w+");

		switch($method){
			
			default: 

				fwrite($a, $content); 
			break;

			case 'begin': 

				fwrite($a, $contentFile . $content);
			break;
			
			case 'end': 

				fwrite($a, $content . $contentFile);
			break;
		}
		fclose($a);
	}

	public static function is_url($str){

		if(preg_match_all('/\b(https?|ftp|file):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|]/i', $str, $all))
			return true;
		else
			return false;
	}
}
?>