<?php
/*
	Manage Session
	SeoSession - Cretate and alter session more easly..
	Create a cookies..
	Create a session

*/
class SeoSession{

	private static $session = false;

	private static $idsession;

	private static $lifetime;

	
	public static function init(){

		if(@session_start())
			self::$session = true;
	}

	#Get session_var value
	public static function get($name){

		if(!self::$session)
			self::init();

		return isset($_SESSION[ $name ]) ? $_SESSION[ $name ] : false;
	}

	public static function set($name, $value = null){

		if(!self::$session)
			self::init();

		return $_SESSION[ $name ] = $value;
	}
}
?>