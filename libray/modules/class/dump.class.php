<?php
/*
	SeoDump = Console Log
	Public Reserved by ifSystem at:
	10 March of 2013
	Version 1.0v

*/

class SeoDump{

	protected static $_sapi = null;
	
 	public static function getSapi(){

        if (self::$_sapi === null) {
        
            self::$_sapi = PHP_SAPI;
        }
        return self::$_sapi;
    }

	#Equal of dump..
	 public static function dump($var, $label = null, $echo = true){
        
        // format the label
        $label = ($label===null) ? '' : rtrim($label) . ' ';

        // var_dump the variable into a buffer and keep the output
        ob_start();
        var_dump($var);
        $output = ob_get_clean();

        // neaten the newlines and indents
        $output = preg_replace("/\]\=\>\n(\s+)/m", "] => ", $output);
        if (self::getSapi() == 'cli') {
            
            $output = PHP_EOL . $label
                    . PHP_EOL . $output
                    . PHP_EOL;
        } else {
            if(!extension_loaded('xdebug')) {
            
                $flags = ENT_QUOTES;
                // PHP 5.4.0+
                if (defined('ENT_SUBSTITUTE')) {
            
                    $flags = ENT_QUOTES | ENT_SUBSTITUTE;
                }
                $output = htmlspecialchars($output, $flags);
            }
            $output = '<pre>'
                    . $label
                    . $output
                    . '</pre>';
        }

        if ($echo) {

            echo($output);
        }
        return $output;
    }	
}
?>