<?php

class SeoTemplate{
	
	/**
	 * Para evitar o uso de 2 nodes XML que são exatamente iguais 
	 * Usar este XML para gerar templates de Links
	*/
	public static $templates = array();

	public static function getTemplate($name){

		self::refresh();

		if(array_key_exists($name, self::$templates)){

			return self::$templates[$name];
		}else{

			return SeoError::cry(ERROR_SEOMARKETING_026, array('template' => $name));
		}
	}

	public static function refresh(){
		
		self::$templates = SeoMarket::getSystem('templates');
	}
}
?>