<?php
/*
	SeoGooglePageRank = SeoGooglePageRank
	Public Reserved by ifSystem at:
	10 March of 2013
	Version 1.0v
	 
*/

require_once('libray/seostats.php');

class SeoGoogle{
	
	public 

	$uri = null, 

	$seostats = false,

	$google;

	public function __construct($uri = null){

		if(!empty($uri))
			return $this->set($uri);	
	}

	public function getPageRank(){

		if(!$this->seostats)
			return 0;

		return $this->google->getPageRank();
	}

	public function getBacklinks(){

		if(!$this->seostats)
			return 0;

		return $this->google->google->getBacklinksTotal();
	}

	public function getSiteIndex(){
		
		if(!$this->seostats)
			return 0;

		return $this->google->getSiteindexTotal();
	}

	public function getPageSpeedScore(){

		if(!$this->seostats)
			return 0;

		return $this->google->getPagespeedScore();
	}

	public function set($uri){

		if(SeoFile::is_url($uri)){

			$this->seostats = new SEOstats($uri);
			$this->google = $this->seostats->Google();
		}else{

			return false;
		}
	}
}
?>