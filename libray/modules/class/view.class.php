<?php
/*
	SeoView - includes content on page with default MVC
	Use controllers and models and views

	Version 1.0
*/
class SeoView{

	#All include by SeoView is phtml
	#Is different not call file..]
	const ext = '.phtml';

	#Save all files include
	private static $includes = array();

	#if check return you include this file
	public static function check($name){

		foreach(self::$includes as $include){

			if($name == $include)
				return true;
		}

		return false;
	}

	public static function addView($file){

		$name = APPLICATION_PATH . VIEW_PATH . $file . self::ext;

		if(!self::check($file) && file_exists($name)){
			require($name);
			self::$includes[] = $file;
		}else{
			return SeoError::cry(ERROR_SEOMARKETING_021, array(
															'view'=>$file)
			);
		}
	}
}?>