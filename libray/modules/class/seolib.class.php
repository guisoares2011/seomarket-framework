<?php
/*
	SEO - Application generator
	Public Reserved by ifSystem at:
	10 March of 2013
	Version 1.0.0.0.1v

	Description:

	The project will help all developers in the industry SEO website with keywords and description, 
	data analysis, among others

	# Open modules start all services if exec others commands after this print ERROR_SEO_1
	main();


*/
include 'trait.php';
class SEOlib extends ExtendTrait{
	
	#use SeoMarketingTrait, resourcesTrait, supportTrait, callTrait;

	public static function loaderConfig($ini){
		
		self::$resources = self::parse_ini_file_ob(LIBRAY_PATH . CONFIG_PATH . self::$ini);
	}

	#Execute function before var or function requested
	public static function actionBefore($a,$b,$c){

	}

	#Execute function after var or function requested
	public static function actionAfter($a,$b,$c){
		
	}
	
	public static function main() {

		self::open();
		self::loaderConfig(self::$ini);
		self::activeLog();
		self::activePlugins();
		self::activeLanguages();		
		self::activeTemplates();		
	}
}
?>