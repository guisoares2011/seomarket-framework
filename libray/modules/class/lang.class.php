<?php
/**
*@license Languages Framework
*@author Guilherme Soares
*/
class SeoLanguage{

	#Default language..
	public static $lang = 'pt-BR';

	public static $content;

	#Inicializa o SeoLanguage
	public static function init(){


		if(isset($_SESSION['lang'])){
			
			//Get By cookie
			self::$lang = $_SESSION['lang'];
		}
		self::selectLanguage(self::$lang);
	}

	#Seleciona o idioma
	public static function selectLanguage($lang){

		self::refreshContent();

		if(array_key_exists($lang, self::$content))
			self::$lang = $lang;
		 else
			self::$lang = 'pt-BR';

		$_SESSION['lang'] = self::$lang;
	}

	#Verifica se a linguagem custom..existe e retorna um valida
	public static function checkLanguage($lang){

		if(array_key_exists($lang, self::$content))
			return $lang;
		else
			return self::$lang;
	}

	#Escreve texto do composer.xml
	public static function printText($page, $name, $print = true, $lang = ''){

		if(empty($lang))
			$lang = self::$lang;
		else
			$lang = self::checkLanguage($lang);

		#Get content on file /libray/languages/<lang>/composer.json
		if(isset(self::$content[ $lang ][ $page ][ $name ])) 
			
			$content = self::$content[ $lang ][ $page ][ $name ];
		else{
			
			SeoError::cry(ERROR_SEOMARKETING_023, array('page' => $page , 'content' => $name));
			$content = "";
		}

		if($print)
			eval('?>' . $content  . '<?php ');

		return $content;
	}

	#Obtem a varivel DOMDocument do XML
	public static function getXMLNodeLanguage(){

		return self::$content[self::$lang]['XMLDOCUMENT_NODES'];
	}

	#Atualiza os valores do SeoLanguage::$content
	public static function refreshContent(){

		self::$content = SeoMarket::getSystem('lang');
	}

	#Gera os icones para alterar o idioma
	public static function createIconLang(){

		foreach(self::$content as $lang => $contentLang)
			echo str_replace('[lang]', $lang, SeoTemplate::getTemplate('LinkToAlterLang'));
	}
}
?>