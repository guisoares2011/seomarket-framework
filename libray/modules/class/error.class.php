<?php
/*
	SEO - Errors of Application
	Public Reserved by ifSystem at:
	10 March of 2013
	Version 1.0v

*/

class SeoError{
	
	private static $ini = 'error.ini';
	
	public static function main(){
	 
		if($_ERROR = parse_ini_file(LIBRAY_PATH . CONFIG_PATH . self::$ini)){
			
			foreach ($_ERROR as $_NAME => $_VALUE){

				define($_NAME, $_VALUE);
			}
		}else{

			echo 'E003 >> Não foi possivel localizar a páginas de erros';
		}
	}

	public static function getLog(){

		$file = SeoLib::getInfo('log','path') . SeoLib::getInfo('log','name');
		return nl2br(SeoFile::getContents($file));
	}

	#Log application require.. Active Errors
	public static function log($log){

		/* Save log */
		if(SeoLib::$system['save-log'] == true)
			self::logComposer($log);
			
		
		
		/* print log */
		if(SeoLib::getInfo('log','print'))
			echo '<br />' . $log . '<br />';		
		
		SeoLib::setSystem('log', SeoLib::getSystem('log') . $log . '\n');	
		return false;
	}

	#Gerate log line
	public static function logComposer($log){

		$file = SeoLib::getInfo('log','path') . SeoLib::getInfo('log','name');

		$log = SeoMarket::getDate() . ' : ' . $log;
		
		SeoFile::writeFile($file, $log . "\n", 'begin');
	}

	#Alter #file, #Line, #tag 
	public static function cry ($msg, $resources){
		
		$log = $msg;

		foreach($resources as $name => $val)
			$log = str_replace('%{' . $name . '}', $val, $log);
		
		return self::log($log);
	}

	public static function unknow($type, $num){

		return self::cry(ERROR_SEOMARKETING_016, array(
													'type'=>$type, 	
													'line'=>$num));
	}
}
?>