<?php
/*
	Reader PHL
	Developer by Guilherme Soares
	Date 10h of 14 March 2013
	Version 1.004
*/

class SeoPhl{

	private static $vars = array();
	
	public static function readerphl($file){
  
  		if(!is_file($file) || !file_exists($file))
  			return false;

    	$content = file($file);

    	foreach ($content as $num => $line) {

    		foreach(self::$vars as $name => $val)
    			$line = preg_replace('/%'.$name.'\s/', $val, $line); 		

			#Remove string if start #
 	  		$line = trim(
 	  				 preg_replace('/(#(.*)\n|["\'])/', '', 
					 str_replace('  ', ' ', $line)));
	
			
			if(strlen($line) == 0)
				continue;
			
			if(preg_match_all('/(\S+|["\'][^"\']+["\'])/', $line, $d)){
			
				$c = 	 SeoLib::formatHex($d[1][0], 'up');
				$type =  SeoLib::formatHex($d[1][1], 'up');
				$value = isset($d[1][2]) ? SeoLib::formatHex($d[1][2],'down') : null;


				$f = count($d[1]) > 3 ? true : false; 

				$list_resources = array(
					
					array(
		  				'tag' => $value,
		  				'file' => $file,
		  				'type' => $type,
		  				'line' => $num,
		  				'command'=> $c,
		  				'plugin'=>$value,
		  			),
			 	);

				switch($c){
					
					case 'RULE':

						/* This command don't do nothing */
					break;

					case 'PRINT':

						$q = '';
						$i = 1; $l = count($d[1]);
					
						for(; $i < $l ;)
							$q .= $d[ 1 ][ $i++ ].' ';

						echo $q;
					break;

					case 'VAR':
						$q = '';
						$i = 2; $l = count($d[1]);
					
						for(; $i < $l ;)
							$q .= $d[ 1 ][ $i++ ].' ';

						$q = trim($q);
						self::$vars[ strtolower($type) ] = $q;
					break;

					case 'GETVAL':
						
						switch($type){
						
							case 'TAG':
								case 'SET':

								if($type == 'TAG'){

									$v = SeoDom::querySelector($value)->nodeValue;	
								}elseif ($type == 'SET'){

									$v = $value;
								}
								

								if($f == true){

							   		$q = ''; $l = count($d[1]);
							   		$i = 3;

							   		for(; $i < $l ;)
							   			$q .= $d[ 1 ][ $i++ ].' ';


							   		$q = trim($q);
							   		switch($q){

							   		 	default:
						 		
						  					if(preg_match_all('/(\S+)/', $q, $args)){

						  						$clause = trim($args[1][0]);
						  						$action = trim($args[1][1]);
						  						$research = isset($args[1][2]) ? trim($args[1][2]) : '';
						  						$explore = isset($args[1][3]) ? trim($args[1][3]) : '';


						  						switch($clause){

						  							case 'ADDVAR':
						  								if(!empty($research) && !empty($explore)){

						  									switch($research){
						  										
						  										case 'ONCALL':
						  										
						  											self::$vars[ strtolower($action) ] = SeoLib::{$explore}($v); 
						  										break;
						  									}	
						  								}else{

						  									self::$vars[ strtolower($action) ] = $v;
						  								}
						  							break;

						  							case 'CALL':
														
														SeoLib::{$action}($v);  													
						  							break;

						  						}
						  					}
						  				break;
						  			}
							   	}
							break;
						}
					break;

					case 'CALL':
						$q = '';
						$i = 1; $l = count($d[1]);
					
						for(; $i < $l ;)
							$q .= $d[ 1 ][ $i++ ].' ';

						$q = trim($q);

						$args = explode(',', $q);

						foreach($args as $key => $v){

							$args[ $key ] = trim($v);
						}

						call_user_func_array('SeoLib::' . strtolower($type), $args);

					break;

					case 'REQUIRE':

						switch($type){
							
							case 'TAG':

							$tag = SeoDom::querySelectorAll($value);

					 			if($f == true){

							   		$q = ''; $l = count($d[1]);
							   		$i = 3;

							   		for(; $i < $l ;)
							   			$q .= $d[ 1 ][ $i++ ] . ' ';


							   		$q = trim($q);

							   		switch($q){

							   		 	default:
						 		
						  					if(preg_match_all('/(\S+)/', $q, $args)){

						  						$clause = trim($args[1][0]);
						  						$article = trim($args[1][1]);
						  						$response = trim($args[1][2]);

						  						switch($clause){

						  							case 'ATTR':
						  								$tag = SeoDom::querySelectorAll($value . SeoDom::str_attr($article, $response));
						  								
						  								if($tag->length < 1){
						  									
						  									SeoResponse::addResponse(ERROR_SEOMARKETING_015, array(
   						  										'attr_name' => $article,
   						  										'attr_value' => $response,
   						  										'tag'  => $value,
   						  										'file' => $file,
   						  										'line' => $num,
   						  									));
						  								}
						  							break;
						  						}
						  					}else{
   						   					
   						   						if($tag->length < 1){
   						 
   						  							SeoResponse::addResponse(ERROR_SEOMARKETING_012, $list_resources[0]);
   						   						} 					
						  					}
						 				break;
									 
								 		case 'NOREPEAT':
						  					
						  					if($tag->length != 1){
						   					
						   						SeoResponse::addResponse(ERROR_SEOMARKETING_012, $list_resources[0]);
						  					}
						 		 		break;

						 				case 'COUNT_PAIR':
						
											if(( $tag->length < 1 ) || ( $tag->length % 2 != 0 )){
											
												SeoResponse::addResponse(ERROR_SEOMARKETING_013, $list_resources[0]);
											}
										break;

										case 'COUNT_ODD':
						
											if(( $tag->length < 1 ) || ( $tag->length % 2 != 1 )){
											
												SeoResponse::addResponse(ERROR_SEOMARKETING_014, $list_resources[0]);
											}
										break;
									}
								}else{

									if($tag->length < 1){

										SeoResponse::addResponse(ERROR_SEOMARKETING_012, $list_resources[0]);
									}
								}
							break;

							case 'PLUGIN':

								if(!array_key_exists($value, SeoLib::getsystem('rules'))){

									SeoError::cry(ERROR_SEOMARKETING_017, $list_resources[0]);
								}
							break;

							default:

								SeoError::unknow($type, $num);
							break;
						}					
					break;

					case 'SECTION':

						switch($type){
							
							case 'ORDER':
								$tags = SeoDom::querySelectorAll($value);
								$els = $next = array();
								$name = explode(',', $value);
								$length = count($name);
								$e = 0;
								
								foreach($name as $key => $el){
									
									$i = $key+2;
									$els[ $el ][ 'nodename' ] = $el;
									$els[ $el ]['forbiden'] = array();

									for(; $i < $length ;){
									
										$els[ $el ]['forbiden'][ ] = $name[ $i++ ]; 
									}
				
								}

								foreach($tags->item as $key => $el){
									$nodename = $el->nodeName;
									
									foreach($next as $val){

										if($val == $nodename){

											SeoResponse::addResponse(ERROR_SEOMARKETING_020, array(
   						  						'tag'  => $nodename,
   						  						'outer' => $el->ownerDocument,
   						  						'file' => $file,
   						  						'line' => $num,
   						  						'article' => 'Headers Rules',
   						  					));											
										}

									}

									$next = $els[ $nodename ]['forbiden'];
								}

							break;

							default:

								SeoError::unknow($type, $num);
							break;
						}
					break;

					default:

						SeoError::cry(ERROR_SEOMARKETING_019, $list_resources[0]);
					break;
				}
			}
		}
	}
}
?>