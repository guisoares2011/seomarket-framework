
<?php
/***
 * Compatibilidade com a versao PHP => 5.3
 * Colocando tudo em uma Class ExtendTrait 
***/
 class ExtendTrait{

 	public static

	#Name file of config
	$ini = 'seomarket.ini',

	#Resources give all values of ini
	$resources = array(),

	#Start main()
	$init = false,

	$system = array(

		'lang' => array(),
		'rules' => array(),
		'templates' => array(),
		'save-log' => false,
		'log' => '',
		'ip' => null,
		'date' => null,
		
		'file' => array(
			'size' => null,
			'name' => null,
			'type' => null,
			'lang' => null,
			'tags' => null,
			'text' => null,
			'lines' => null,
			'links' => null,
			'content' => null,
			'doctype' => null,
			'headers' => null,
			'keywords' => null,
			'description' => null
		),		
	);

 	#Open gateway of execute others functions
	public static function open(){

		return self::$init = true;
	}

	public static function reader(){

		if(SeoDom::$dom != false){
			foreach(self::$system['rules'] as $name => $value){
				
				SeoPhl::readerphl($value);
			}
		}else{
			SeoError::log(ERROR_SEOMARKETING_018);
		}
	}

	#Active or not save logs
	public static function activeLog(){

		if(self::getInfo('log','createlog') == true){
		
			self::$system['save-log'] = true;

		}else{
			
			self::$system['save-log'] = false;

		}
	}

	#Loader languages ini
	#Configure idiomas in SeoMarket.ini
	public static function activeLanguages(){
		
		$lang = explode(' ', self::getInfo('lang', 'avaliable'));

		foreach($lang as $key => $val){
			
			$LangContent = array();
			$file = self::getInfo('lang', 'path') . $val . '/composer.xml';
			
			if(file_exists($file)){
				
				#Reader a XML and get nodex page
				$xml = new DOMDocument;
				$xml->load($file);
				$pages = $xml->getElementsByTagName('page');
				
				foreach($pages as $page){

					$namePage = $page->getAttribute('name-page');
					$contents = $page->getElementsByTagName('content');
					foreach($contents as $content){

						$NameContent = $content->getAttribute('name');
						$nodeValue = $content->nodeValue;

						$LangContent[ $namePage ][ $NameContent ] = $nodeValue;
					}
				}
				#If you need XML of language you can this to push your informations but
				#need know DOMDocument
				$LangContent['XMLDOCUMENT_NODES'] = $xml;
				self::$system['lang'][ $val ] = $LangContent;
				
				// self::$system['lang'][ $val ] = json_decode( SeoFile::getContents($file) );
				
			}else{

				return SeoError::cry(ERROR_SEOMARKETING_006 , array('lang' => $val));	
			}
		}
	}

	public static function activeTemplates(){

		$active = self::getInfo('templates', 'active');

		if($active == 1){

			$file = self::getInfo('templates', 'path') . self::getInfo('templates','name');

			if(file_exists($file)):
				$xml = new DOMDocument;
				$xml->load($file);
				$templates = $xml->getElementsByTagName('template');
	
				foreach($templates as $template){

					$name = $template->getAttribute('name');					
					$nodeValue = $template->nodeValue;
					self::$system['templates'][$name] = $nodeValue;
				}
			else:
				return SeoError::cry(ERROR_SEOMARKETING_025, array('file' => $gile));
			endif;
		}
	}

	public static function activePlugins(){
		
		foreach(self::getInfo('plugins') as $name => $value){
			
			if(preg_match_all('/^extension\.([a-z]+)$/', $name, $match)){
				
				$nameext = explode('.', $name);
				$nameext = $nameext[1];

				$file = self::getInfo('plugins','path') . $value; 
				
				if(file_exists($file)){

					self::$system['rules'][ $nameext ] = $file;	
				
				}else{

					return SeoError::cry(ERROR_SEOMARKETING_006 , array('lang' => $val));	
				}
			}
		}
	
	}	

	#Parse ini file to Array and after obje
	public static function parse_ini_file_ob($file){

		if(	$inc = @parse_ini_file($file, true) ){

			foreach($inc as $name => $section)
				foreach($section as $article => $value){

					$value = str_replace(
					array('LOG_PATH',    'VIEW_PATH',
						  'CONFIG_PATH', 'PLUGIN_PATH',
						  'LANG_PATH',   'MODULES_PATH',
						  'LIBRAY_PATH', 'CONTROLLERS_PATH',
						  'MODELS_PATH', 'APPLICATION_PATH', 'TEMPLATES_PATH'),
					array(LOG_PATH,    VIEW_PATH,
						  CONFIG_PATH, PLUGIN_PATH,
						  LANG_PATH,   MODULES_PATH,
						  LIBRAY_PATH, CONTROLLERS_PATH,
						  MODELS_PATH, APPLICATION_PATH, TEMPLATES_PATH), 
								 $value);
					
					if($article == 'path')
						$value = str_replace(' ', '', $inc[ $name ][ $article ]);

					$inc[ $name ][ $article ] = $value;
				}
			
			
			$inc = (object) $inc;

			foreach($inc as $section => $value)
				$inc->{$section} = (object)$inc->{$section};
			
			return (object)$inc;
		}else{

			return SeoError::log(ERROR_SEOMARKETING_004);	
		}
	}	


	#Get info of System..
	public static function getSystem($section){
	
		return self::$system[ $section ];
	}


	#Get information of ini
	public static function getInfo($section, $article = null ){
		
		if(empty($article)):
			if(isset(self::$resources->{$section})):

				return self::$resources->{$section};
			else:

				return null;
			endif;
		else:
			if(isset(self::$resources->{$section}->{$article})):
			
				return self::$resources->{$section}->{$article};
			else:
				
				return null;
			endif;
		endif;
	}

	#Get ip of user
	public static function getIp(){

		return self::$system['ip'] = $_SERVER["REMOTE_ADDR"];
	}

	#Get actually date
	public static function getDate(){

		return self::$system['date'] = date("Y-m-d H:i:s");
	}


	#Insert or alter information in system var
	public static function setSystem($section, $values = ""){
	
		if(is_array($values))
			foreach($values as $name => $val)
				self::$system[ $section ][ $name ] = $val;

		else
			self::$system[ $section ] = $values;	
	}

	public static function useragent(){

        return (isset($_SERVER["HTTP_USER_AGENT"]) && 0 < strlen($_SERVER["HTTP_USER_AGENT"])) ?
            $_SERVER["HTTP_USER_AGENT"] : null;
    }

	public static function removeSpaces($s){

		return 	trim(preg_replace('/[\v\t\r\s]{1,}/'," ",$s));
	}

	public static function formatHex($string, $resources = 'not'){

		$a = array('\x20'=>' ');

		foreach($a as $hex => $val)
			$string = str_replace($hex, $val, $string);
		
		switch($resources){

			case 'up': 	$string = strtoupper($string); break;
			case 'down': $string = strtolower($string); break;
		}

		return trim($string);
	}
	
	public static function activeErrors(){

	}

	public static function activeViews(){

	}
 }
?>