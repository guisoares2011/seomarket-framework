<?php
/**
*	SEO -  Trait Support Seo Market
*	Public Reserved by ifSystem at:
*	@date 10 March of 2013
*	@version 1.0.4v
*   
*/
Trait resourcesTrait{

	public static

	#Name file of config
	$ini = 'seomarket.ini',

	#Resources give all values of ini
	$resources = array(),

	#Start main()
	$init = false,

	$system = array(

		'lang' => array(),
		'rules' => array(),
		'save-log' => false,
		'log' => '',
		'ip' => null,
		'date' => null,
		
		'file' => array(
			'size' => null,
			'name' => null,
			'type' => null,
			'lang' => null,
			'tags' => null,
			'text' => null,
			'lines' => null,
			'links' => null,
			'content' => null,
			'doctype' => null,
			'headers' => null,
			'keywords' => null,
			'description' => null
		),		
	);
}

?>