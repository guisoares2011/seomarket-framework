<?php
/*
	SEO -  Trait Support Seo Market
	Public Reserved by ifSystem at:
	10 March of 2013
	Version 1.0v

*/
Trait supportTrait{

	#Parse ini file to Array and after obje
	public static function parse_ini_file_ob($file){

		if(	$inc = @parse_ini_file($file, true) ){

			foreach($inc as $name => $section)
				foreach($section as $article => $value){

					$value = str_replace(
					array('LOG_PATH',    'VIEW_PATH',
						  'CONFIG_PATH', 'PLUGIN_PATH',
						  'LANG_PATH',   'MODULES_PATH',
						  'LIBRAY_PATH', 'CONTROLLERS_PATH',
						  'MODELS_PATH', 'APPLICATION_PATH'),
					array(LOG_PATH,    VIEW_PATH,
						  CONFIG_PATH, PLUGIN_PATH,
						  LANG_PATH,   MODULES_PATH,
						  LIBRAY_PATH, CONTROLLERS_PATH,
						  MODELS_PATH, APPLICATION_PATH), 
								 $value);
					
					if($article == 'path')
						$value = str_replace(' ', '', $inc[ $name ][ $article ]);

					$inc[ $name ][ $article ] = $value;
				}
			
			
			$inc = (object) $inc;

			foreach($inc as $section => $value)
				$inc->{$section} = (object)$inc->{$section};
			
			return (object)$inc;
		}else{

			return SeoError::log(ERROR_SEOMARKETING_004);	
		}
	}	


	#Get info of System..
	public static function getSystem($section){

		return self::$system[ $section ];
	}


	#Get information of ini
	public static function getInfo($section, $article = null ){
		
		if(empty($article)):
			if(isset(self::$resources->{$section})):

				return self::$resources->{$section};
			else:

				return null;
			endif;
		else:
			if(isset(self::$resources->{$section}->{$article})):
			
				return self::$resources->{$section}->{$article};
			else:
				
				return null;
			endif;
		endif;
	}

	#Get ip of user
	public static function getIp(){

		return self::$system['ip'] = $_SERVER["REMOTE_ADDR"];
	}

	#Get actually date
	public static function getDate(){

		return self::$system['date'] = date("Y-m-d H:i:s");
	}


	#Insert or alter information in system var
	public static function setSystem($section, $values = ""){
	
		if(is_array($values))
			foreach($values as $name => $val)
				self::$system[ $section ][ $name ] = $val;

		else
			self::$system[ $section ] = $values;	
	}

	public static function useragent(){

        return (isset($_SERVER["HTTP_USER_AGENT"]) && 0 < strlen($_SERVER["HTTP_USER_AGENT"])) ?
            $_SERVER["HTTP_USER_AGENT"] : null;
    }

	public static function removeSpaces($s){

		return 	trim(preg_replace('/[\v\t\r\s]{1,}/'," ",$s));
	}

	public static function formatHex($string, $resources = 'not'){

		$a = array('\x20'=>' ');

		foreach($a as $hex => $val)
			$string = str_replace($hex, $val, $string);
		
		switch($resources){

			case 'up': 	$string = strtoupper($string); break;
			case 'down': $string = strtolower($string); break;
		}

		return trim($string);
	}
}
?>