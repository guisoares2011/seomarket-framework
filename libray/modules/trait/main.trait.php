<?php
/*
	SEO -  Trait Seo Market
	Public Reserved by ifSystem at:
	10 March of 2013
	Version 1.0v

*/
Trait SeoMarketingTrait{

	#Open gateway of execute others functions
	public static function open(){

		return self::$init = true;
	}

	public static function reader(){

		if(SeoDom::$dom != false){
			foreach(self::$system['rules'] as $name => $value){
				
				SeoPhl::readerphl($value);
			}
		}else{
			SeoError::log(ERROR_SEOMARKETING_018);
		}
	}

	#Active or not save logs
	public static function activeLog(){

		if(self::getInfo('log','createlog') == true){
		
			self::$system['save-log'] = true;

		}else{
			
			self::$system['save-log'] = false;

		}
	}

	#Loader languages ini
	public static function activeLanguages(){
		
		$lang = explode(' ', self::getInfo('lang', 'avaliable'));

		
		foreach($lang as $key => $val){
			
			$file = self::getInfo('lang', 'path') . $val . '/composer.json';
			
			if(file_exists($file)){
				
				self::$system['lang'][ $val ] = json_decode( SeoFile::getContents($file) );
			}else{

				return SeoError::log(ERROR_SEOMARKETING_006 . $val);	
			}
		}
	}


	public static function activePlugins(){
		
		foreach(self::getInfo('plugins') as $name => $value){
			
			if(preg_match_all('/^extension\.([a-z]+)$/', $name, $match)){
				
				$nameext = explode('.', $name);
				$nameext = $nameext[1];

				$file = self::getInfo('plugins','path') . $value; 
				
				if(file_exists($file)){

					self::$system['rules'][ $nameext ] = $file;	
				
				}else{

					return SeoError::log( ERROR_SEOMARKETING_006 . $value );
				}
			}
		}
	
	}	

	public static function activeErrors(){

	}

	public static function activeViews(){

	}
}

?>