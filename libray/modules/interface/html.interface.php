<?php
/*
	Interface - Html
	Contains all constants for create code HTML and CSS
*/
Interface Html{

	/**
	*@param To start element tag <tag>
	*/const open_tag  = '<';
	
	/**
	*@param To finish element tag <tag>
	*/const close_tag = '>';
	
	/**
	*@param To insert bar to close tags 
	*/const bar = '/';

	/**
	*@param To Attribute
	*/const equal = '=';

	/**
	*@param Aspas Duplas
	*/const as2 = '"';

	/**
	*@param Aspas Simples
	*/const as1 = '\'';
}
?>