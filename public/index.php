<?php
/*
	Boot Strap Version 1.0
	Use becase require very files then 
	Create one BootStrap to include this files and autoloader.

	To add one trait add alter private tratit
	or
	Add class alter var private classes;
*/

class boot{
	
	public static $uri;

	public static $interfaces = 'html';

	// public static $traits = 'main|support|resources|call';
	
	public static $classes = 'controller|lang|templates|file|dump|dom|error|criteria|response|seolib|host|session|view|xml|model';
		
	const 
	_class_ = 'class',
	_trait_ = 'trait',
	_interface_ = 'interface',
	_ds_ = '/',
	_php_ = 'php';

	public static $Framework;

	public static function load(){

		session_start();
		
		$uri = $_SERVER['REQUEST_URI'];
		if($uri == '/' || empty($uri)){

			self::$uri = array();
		}else{

			self::$uri = explode('/', $_SERVER['REQUEST_URI']);	
		}
		define('LOG_PATH'         , 'logs/');
		define('VIEW_PATH'        , 'views/');
		define('CONFIG_PATH'      , 'config/');
		define('PLUGIN_PATH'      , 'plugins/');
		define('LANG_PATH'        , 'languages/');
		define('MODULES_PATH'     , 'modules/');
		define('LIBRAY_PATH'      , '../libray/');
		define('CONTROLLERS_PATH' , 'controllers/');
		define('MODELS_PATH' 	  , 'models/');
		define('APPLICATION_PATH' , '../application/');
		define('TEMPLATES_PATH' , 'templates/');
		self::addInterfaces();
		// self::addTraits();
		self::addClasses();

		SeoError::main();
	}

	public static function addInterfaces(){
		
		$array = explode('|', self::$interfaces);		
		self::parseArray($array, self::_interface_);
	}

	public static function addClasses(){
		
		$array = explode('|', self::$classes);		
		self::parseArray($array, self::_class_);
	}

	// #Load traits
	// public static function addTraits(){

	// 	$array = explode('|', self::$traits);
	// 	self::parseArray($array, self::_trait_);			
	// }

	#Foreach array and execute require
	public static function parseArray($array, $application){

		foreach($array as $element)			
			self::requires($element, $application);
	}

	public static function requires($file, $type){
		require(
			LIBRAY_PATH 
			. MODULES_PATH 
			. $type . self::_ds_ 
			. $file	. '.' . $type . '.' . self::_php_);	
	}

	public static function init(){

		require(LIBRAY_PATH . 'seomarket.php');
	}

	public static function caption($str){
		$str[0] = strtoupper($str);
		return $str;
	}

	//Chama o Controller...
	public static function playController(){
		
		/**** Modulos Obrigatorios ***/
		SeoModel::addModel('Lang/select');

		$value = self::getValue();
		$action = self::getAction();
		$controller = self::caption(self::getController());

		SeoSession::init();
		if($controller == 'Home')
			$controller = 'Index';
		
		if(($controller != 'AcceptUserAdmin') || ($action != 'CreateSession')){
			if((!isset($_SESSION['USER_AUTH_PROFILE'])) ||($_SESSION['USER_AUTH_PROFILE'] != true))
				$controller = 'SoonBuild';
		}
	
		$file = APPLICATION_PATH . CONTROLLERS_PATH . $controller . 'Controller.php';
		
		if(!file_exists($file)){
			
			$controller = 'Error';
			$file = APPLICATION_PATH . CONTROLLERS_PATH . $controller . 'Controller.php';
		}

		//Chama o arquivo controller
		require($file);	
		
		$className = $controller . 'Controller';
		$class = new $className;
		
		self::$Framework = $class;

		$actionName = $action . 'Action';
		#Exec init();

		#Event List
		$class->beforeBuffer();
		$class->init($value, $action);


		if(method_exists($className, $actionName)){

			$class->{$actionName}($value);
		}else{
			//Exists function that não tem IndexAction como
			//A ErrorController só possui init();
			if(method_exists($className, 'IndexAction'))
				$class->IndexAction($value);
		}
		$class->beforeGetBuffer();
		$class->getBuffer();
		$class->afterGetBuffer();
	}

	public static function getController(){

		return isset(self::$uri[1]) && !empty(self::$uri[1]) 
			? self::$uri[1] 
			: 'Index';
	}

	public static function getAction(){

		return isset(self::$uri[2]) && !empty(self::$uri[2]) 
			? self::$uri[2] 
			: 'index';
	}

	public static function getValue(){

		$vars = array();

		if(isset(self::$uri[3]) && !empty(self::$uri[3])){
			parse_str(self::$uri[3], $vars);
		}
		
		return $_GET = $vars;
	}
}
boot::load();
boot::init();
boot::playController();
?>